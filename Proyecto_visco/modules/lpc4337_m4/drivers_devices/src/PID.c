/*==================[inclusions]=============================================*/

#include "PID.h"       /* <= own header */
#include "systemclock.h"
#include <stdio.h>
#include <stdint.h>
#include "gpio.h"


/*==================[macros and definitions]=================================*/

float Kp=60, Ki=0, Kd=0;
int output_bits = 8;
int16_t setpoint = 950;
int controlando =1 ;
bool output_signed = false;

const byte numChars = 16;
char receivedChars[numChars];
char tempChars[numChars];        // temporary array for use when parsing

char commandFromPC[numChars] = {0};
char parameterFromPC[numChars] = {0};

boolean newData = false;


volatile unsigned int t_counter;
volatile unsigned int a_counter;
volatile byte bite,byteval;
volatile byte *tmppasos;


FastPID myPID(Kp, Ki, Kd, RATE, output_bits, output_signed);


/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

 if( (time_actual-time_last_actualizacion) >= INV_RATE){
  
  time_last_actualizacion = time_actual;
  
  temp_counter++;
  temp_counter &= 0b11111;
  acum_temp -= temp_array[temp_counter];
  temp_array[temp_counter] = analogRead(THERMISTOR_PIN);
  acum_temp += temp_array[temp_counter];
  adc_temp = acum_temp >> 5;
//  Serial.print(adc_temp);Serial.print(" "); Serial.println(temp_array[temp_counter]);
  if(controlando){
    if((setpoint-adc_temp) > control_window){
      power = 0;
    }
    else if((adc_temp-setpoint) > control_window){
      power =255;
    }
    else{
  // Como el termistor es NTC y el valor medido por el ADC es proporcional  la Resistencia del Termistor, entonces tengo que pasarle los valores multiplicados por -1
    power = myPID.step(-setpoint, -adc_temp);
    }
    analogWrite(HEATER_PIN, power );
   
  }

