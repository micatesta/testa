/* Copyright 2021,
 * Micaela Testa
 * mica.testa@gmail.com
 * Maestría en Ingeniería Biomédica
 * Facultad de Ingenieria
 * Universidad Nacional de Entre Rios
 * Argentina
 * All rights reserved.
 */




/*==================[inclusions]=============================================*/



/*==================[macros and definitions]=================================*/


#define HX711_CLK GPIO4 //pines del amplificador de presion
#define HX711_DAT GPIO6


#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif


/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/




