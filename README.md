# Analizador de viscoelasticidad de un coágulo de sangre

Una prueba viscoelástica es un método de análisis hemostático que proporciona una visualización en tiempo real de la coagulación ex-vivo. Permite el examen de las contribuciones de las proteínas plasmáticas y celulares a la coagulación, incluido el número y la función de las plaquetas, la función de la fibrina y la función del factor de coagulación.

El método evalúa las propiedades físicas del coágulo durante la transición de la sangre de un estado líquido a un gel (sólido elástico), ya sea mediante la medición del módulo de corte del coágulo usando transducción de fuerza física o mediante la medición de la frecuencia de resonancia del coágulo usando interrogación sonométrica. Los resultados se informan en una traza en vivo, con diferentes parámetros de ésta que reflejan diferentes contribuyentes a la hemostasia. Estos parámetros informados varían entre las plataformas de prueba.

En el VILAS, dispositivo desarrollado por MZP Tecnología, se pretende evaluar la coagulación de la sangre a través de la lectura de la variación de presión de la muestra ante la aplicación de una excitación senoidal constante (ejercida mediante una jeringa). Se medirá la temperatura del sistema ya que la sangre debe mantenerse a 37ºC. 


**Objetivo de la aplicación**

- Aplicación de Sistemas embebidos con la plataforma [EDU-CIAA](www.proyecto-ciaa.com.ar/) basada en LPC4337JBD144. 

- Medir la presión de un coágulo de sangre al excitarlo mediante un movimiento constante.

- Medir la temperatura de la muestra durante su medición. 


**Diagrama en bloques**

![diagrama](  Diagrama_en_bloque__1_.png "imagen")

**Listado de periféricos a utilizar (marca, modelo y driver dispuesto)**

- Sensores de presión analógicos: PX26 SERIES Honeywell

- Termistores 100k
