
#include <chip.h>
#include <stopwatch.h>
#include <stdint.h>

const uint32_t ExtRateIn = 0;
const uint32_t OscRateIn = 12000000;

#define BUFFLEN 128

RINGBUFF_T rbTx, rbRx;
uint8_t txBuff[BUFFLEN], rxBuff[BUFFLEN];

void UART2_IRQHandler(void){
  Chip_UART_IRQRBHandler(LPC_USART2,
    &rbRx, &rbTx);}

int main(void)
{
  unsigned char txt[] = "Hola mundo!\r\n";
  unsigned char rxData;
  Chip_SetupXtalClocking();
  SystemCoreClockUpdate();
  fpuInit();
  StopWatch_Init();

  // Led RGB
   Chip_SCU_PinMuxSet(0x02, 0x0A, SCU_MODE_INACT | SCU_MODE_FUNC0);
   Chip_SCU_PinMuxSet(0x02, 0x0B, SCU_MODE_INACT | SCU_MODE_FUNC0);
   Chip_SCU_PinMuxSet(0x02, 0x0C, SCU_MODE_INACT | SCU_MODE_FUNC0);

   // Led P2_0
   Chip_SCU_PinMuxSet(0x02, 0x00, SCU_MODE_INACT | SCU_MODE_FUNC4);
   // Led P2_1
   Chip_SCU_PinMuxSet(0x02, 0x01, SCU_MODE_INACT | SCU_MODE_FUNC4);
   // Led P2_2
   Chip_SCU_PinMuxSet(0x02, 0x02, SCU_MODE_INACT | SCU_MODE_FUNC4);

  Chip_GPIO_Init(LPC_GPIO_PORT);

  Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 0);
  Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,    5, 0);

  Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 1);
  Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,    5, 1);

  Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 2);
  Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,    5, 2);

  Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 0, 14);
  Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,    0, 14);

  Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 1, 11);
  Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,    1, 11);

  Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 1, 12);
  Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,    1, 12);

  Chip_SCU_PinMuxSet(7, 1, SCU_MODE_PULLDOWN | SCU_MODE_FUNC6);
  Chip_SCU_PinMuxSet(7, 2, SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS | SCU_MODE_FUNC6);

  RingBuffer_Init(&rbRx,rxBuff,1,BUFFLEN);
  RingBuffer_Init(&rbTx,txBuff,1,BUFFLEN);

  Chip_UART_Init(LPC_USART2);
  Chip_UART_ConfigData(LPC_USART2, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT | UART_LCR_PARITY_DIS);
  Chip_UART_SetBaud(LPC_USART2, 115200);
  Chip_UART_IntEnable(LPC_USART2, UART_IER_RBRINT);
  NVIC_EnableIRQ(USART2_IRQn);
  Chip_UART_TXEnable(LPC_USART2);
  Chip_UART_SendRB(LPC_USART2, &rbTx, txt, strlen(txt));
  while (1){
	 StopWatch_DelayMs(300);
	 Chip_GPIO_SetPinToggle(LPC_GPIO_PORT, 1, 11);
	 while(Chip_UART_ReadRB(LPC_USART2,&rbRx, &rxData, 1) == 1)
	    Chip_UART_SendRB(LPC_USART2, &rbTx, &rxData, 1);
  }
  return 0;
}
