unsigned int backup_OCR1A;
byte backup_TCCR1B;

ISR(TIMER1_COMPA_vect){
  jumpTable[isrSelect]();
}

void dummy() {
}

//esta rutina setea el timer1 para que las interrupciones ocurran cada x microsegundos.
// En otro lado hay que configurar el timer para que trabaje en Modo 4 (CTC)

void set_timer1(unsigned long microseconds){
byte clockSelectBits;
unsigned long cycles = (F_CPU/1000000) * microseconds;

#ifdef DEBUG
Serial.println("set_timer1");
Serial.print("cycles1 ");
Serial.println(cycles,HEX);
#endif

if(cycles < 0xFFFF)              clockSelectBits = _BV(CS10);              // no prescale, full xtal
  else if((cycles >>= 3) < RESOLUTION) clockSelectBits = _BV(CS11);              // prescale by /8
  else if((cycles >>= 3) < RESOLUTION) clockSelectBits = _BV(CS11) | _BV(CS10);  // prescale by /64
  else if((cycles >>= 2) < RESOLUTION) clockSelectBits = _BV(CS12);              // prescale by /256
  else if((cycles >>= 2) < RESOLUTION) clockSelectBits = _BV(CS12) | _BV(CS10);  // prescale by /1024
  else        cycles = RESOLUTION - 1, clockSelectBits = _BV(CS12) | _BV(CS10);  // request was out of bounds, set as maximum

  OCR1A = cycles;
  // apago todos los bits de seleccion de prescaler
  TCCR1B &= ~(_BV(CS10) | _BV(CS11) | _BV(CS12));
  //prendo los bits de prescaler calculado
  TCCR1B |= clockSelectBits;  
#ifdef DEBUG
    Serial.print("cycles2 "); 
    Serial.println(cycles,HEX);
    Serial.print("OCR1AH ");
    Serial.println(OCR1AH,HEX);
    Serial.print("OCR1AL ");
    Serial.println(OCR1AL,HEX);
    Serial.print("OCR1A ");
    Serial.println(OCR1A,HEX);

    
byte  prescaler = TCCR1B & 0b111;
  Serial.println(prescaler,BIN);                      
#endif
}

unsigned long counts_from_timer1(){
  byte prescaler;
  unsigned long cycles;
  cycles = OCR1A;
  prescaler = TCCR1B & 0b111;
#ifdef DEBUG
  Serial.println("counts_from_timer1");
  Serial.print("cycles1 ");
  Serial.println(cycles,BIN);

  Serial.println(prescaler,BIN);
#endif

  if (prescaler == 0b010) cycles <<= 3;
  if (prescaler == 0b011) cycles <<= 6;
  if (prescaler == 0b100) cycles <<= 8;
  if (prescaler == 0b101) cycles <<= 10;
  
#ifdef DEBUG  
  Serial.print("cycles2 ");
  Serial.println(cycles);
  Serial.println(cycles,BIN);
#endif
return cycles;  
}

void set_timer1_wave(float frec){
  set_timer1( (unsigned long) (122.07 / frec));
}

void backup_timer1(){
  backup_OCR1A  = OCR1A;
  backup_TCCR1B = TCCR1B;
}

void restore_timer1(){
  OCR1A  = backup_OCR1A;
  TCCR1B = backup_TCCR1B;
}

void set_direccion(){
  if (direccion == 1) digitalWrite(DIR_PIN, HIGH);
  else digitalWrite(DIR_PIN, LOW);
}

 

// Rutina que genera periodicamente una serie de pasos (hasta 8192) a partir de los bits cargados en el vector pasos y el byte direcciones 
// Los 8192 eventos pueden dar un paso (bit ==1) o no (bit ==0) y están divididos en 8 segmentos de 1024 eventos c/u. La dirección de c/u de estos segmentos
// está determinado por el bit correspondiente de la variable direcciones.
// A su vez va actualizando el contador global de posición globalpos
// Esta rutina será llamada por la interrupción TIMER1_COMPA_vect 
// Para cambiar la frecuencia es necesario cambiar el periodo de las interrupciones (usando la rutina set_timer1(x)

void timer2_wave(){
  static uint8_t b=0;
  static uint8_t pasoensegmento=0;
  static uint8_t segmento=0;
//  int8_t dir;
  uint16_t paso;
  
  b++;
  b &= 0b111;
  if(!b){
    pasoensegmento++;
    pasoensegmento &= 0b1111111;
  }
  if (!pasoensegmento & !b){
    segmento++;
    segmento &= 0b111; 
  }
  
  direccion = ((direcciones >> segmento) & 0b1 )? 1:-1;
  paso = segmento;
  paso <<= 7;
  paso += pasoensegmento;
  

  if((pasos[paso] >> b) & 0b1) {
    globalpos += direccion;
  
  
    if(direccion == 1){
        digitalWrite(DIR_PIN, HIGH);
      }else{
        digitalWrite(DIR_PIN, LOW);
      }

    digitalWrite(STEP_PIN, HIGH);
    delayMicroseconds(20);
    digitalWrite(STEP_PIN, LOW);
  }
}



inline void halt(void){
  //Paro la generacion de interrupciones poniendo un cero en el bit OCIE1A del TIMSK1
    TIMSK1 &= ~(1 << OCIE1A);
    isrSelect =0;    
   }

inline void go(void){
  // Habilito la generacion de interrupciones.
    TIMSK1 |= (1 << OCIE1A);
   }

inline void pause(void){
    TIMSK1 &= ~(1 << OCIE1A);  
}

void fill_pasos_seno(int ampl){
int i,ac,dir;
double seno;
  for(i=0;i<1024;i++){
    *(pasos+i)=0;
  }
  direcciones = 0xC3;
  ac=0;
  for(i=0;i<8192;i++){
    dir = (direcciones & (1UL << (i>>10)))?1:-1;
    seno=ampl*sin(TWO_PI*i/8192);

    //Serial.print(i);Serial.print(" ");Serial.print(dir);Serial.print(" ");Serial.println(seno);
    if( abs(seno-ac)> 0.5){
      ac += dir;
     // Serial.print(i);Serial.print(" ");Serial.println(ac);

      //set bit i =1
      *(pasos+(i>>3)) |= 1 << (i & 0b111) ;
    }
 //   Serial.print(i);Serial.print(" ");Serial.println(acum);
  }  
}


void timer1_constant_speed(){
if(direccion == 1){
      digitalWrite(DIR_PIN, HIGH);
      globalpos++;
    }else{
      digitalWrite(DIR_PIN, LOW);
      globalpos--;
    }
digitalWrite(STEP_PIN, HIGH);
  delayMicroseconds(20);
digitalWrite(STEP_PIN, LOW);
}

void timer1_segment_constant_speed(){
static int counter;
// flag indica si está corriendo o no
static bool flag=false;

if(!flag){
   counter = num_pasos;
   flag = true;
}
if (counter == 0){
  halt();
  flag= false;
  isrSelect =0;
}
else{
  counter -= 1;
  if(direccion == 1){
        digitalWrite(DIR_PIN, HIGH);
        globalpos++;
      }else{
        digitalWrite(DIR_PIN, LOW);
        globalpos--;
      }
 // Serial.println(counter);
  digitalWrite(STEP_PIN, HIGH);
  delayMicroseconds(20);
  digitalWrite(STEP_PIN, LOW);
}
}


void set_mustep(uint8_t d){
  //Selecciona el microstepping en el chip Allegro A4988 
  if(d ==1){
    digitalWrite(MS0_PIN, LOW);
    digitalWrite(MS1_PIN, LOW);
    digitalWrite(MS2_PIN, LOW);
  }
  else if(d == 2){
    digitalWrite(MS0_PIN, HIGH);
    digitalWrite(MS1_PIN, LOW);
    digitalWrite(MS2_PIN, LOW);
  }
  else if(d == 4){
    digitalWrite(MS0_PIN, LOW);
    digitalWrite(MS1_PIN, HIGH);
    digitalWrite(MS2_PIN, LOW);
  }
  else if(d == 8){
    digitalWrite(MS0_PIN, HIGH);
    digitalWrite(MS1_PIN, HIGH);
    digitalWrite(MS2_PIN, LOW);
  }
  else if(d ==16){
    digitalWrite(MS0_PIN, HIGH);
    digitalWrite(MS1_PIN, HIGH);
    digitalWrite(MS2_PIN, HIGH);
  }
  
}
  
