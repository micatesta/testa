 /* Copyright 2021,
 * Micaela Testa
 * mica.testa@gmail.com
 * Maestría en Ingeniería Biomédica
 * Facultad de Ingenieria
 * Universidad Nacional de Entre Rios
 * Argentina
 * 
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "visco.h"
//#include "PID.h"
//#include "control_temp.h"
#include "HX711.h"
#include "systemclock.h"
#include <chip.h>
#include <stdint.h>
#include "analog_io.h"
#include "systemclock.h"
//#include "switch.h"
#include "uart.h"
//#include "i2c.h"



/*==================[macros and definitions]=================================*/
uint16_t last_adc_value;


/*==================[internal data definition]===============================*/

void adc_interrupt() {
	AnalogInputRead(CH1, &last_adc_value);
	AnalogStartConvertion();
}

analog_input_config adc_config = {
	.input = CH1,
	.mode = AINPUTS_SINGLE_READ,
	.pAnalogInput = &adc_interrupt
};

serial_config UART_USB = {
	.baud_rate = 115200,
	.port = SERIAL_PORT_PC,
	.pSerial = NULL
};



/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/
void SisInit(void)
{
	
	
	//AnalogInputInit(&adc_config);
	
    UartInit(&UART_USB);

	
	//AnalogStartConvertion();
}



/*main*/

int main(){


    int32_t data = 0;
    SisInit();
    SystemClockInit();
    HX711_init();
    data = HX711_read();
    UartSendByte(SERIAL_PORT_PC, &data);
    
    while(1){
		
		
	}


  return(0);
}

/*==================[end of file]============================================*/

