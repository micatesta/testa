//driver para leer la presión a través del amplificador HX711

long ReadCount(void){
 long Count;
 unsigned char i;
// ADDO=1;
// HX711_CLK=0;
 Count=0;
unsigned long t;
t = millis();
//Serial.println("ReadCount");
 while((digitalRead(HX711_DAT)==HIGH) && ((millis()-t) < 15 )) ;
//Serial.println("Start reading");
  for (i=0;i<24;i++){
    digitalWrite(HX711_CLK,HIGH);
    Count=Count<<1;
    digitalWrite(HX711_CLK,LOW);
    if(digitalRead(HX711_DAT)) Count++;
  }
  
  for(i=0;i<gain_bits;i++){
  digitalWrite(HX711_CLK,HIGH);
  digitalWrite(HX711_CLK,LOW);
  }
//completo los bits necesarios del long int (32) 
// add-on Hernan ojo que esto depende de la implementación, en Arduino funciona.
 Count = (Count << 8)>>8;

 //digitalWrite(HX711_CLK,HIGH);

// digitalWrite(HX711_CLK,LOW);
 
 return(Count);
} 
