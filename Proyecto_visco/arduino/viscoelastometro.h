#include <limits.h>
#include <math.h>
#include <FastPID.h>


#define MODEL "VE MZP"
#define VERSION "V0.1"
#define SERIE "S/N 001"
#define MAXARGS 4
#define RESOLUTION 65536UL

#define SERIESRESISTOR 4700
#define THERMISTORNOMINAL 100000
#define BCOEFFICIENT 3950
#define TEMPERATURENOMINAL 25
#define RATE 10
#define INV_RATE 0.1

#define THERMISTOR_PIN A2 
#define HEATER_PIN 11

#define DIR_PIN   9 //direction
#define STEP_PIN  10 //step
#define MS0_PIN 5
#define MS1_PIN 6
#define MS2_PIN 7


#define SLEEP_PIN 5
#define RESET_PIN 6

#define SYNC_PIN 4

#define HX711_CLK 12
#define HX711_DAT 13

void set_timer1(unsigned long);
unsigned long counts_from_timer1(void);
void set_timer1_wave(float);

void set_direccion(void);
void fill_direcciones(void);
void fill_pasos_seno(float);
void set_mustep(uint8_t);
void dummy(void);
void timer1_wave(void);
void timer2_wave(void);
void timer1_constant_speed(void);
void timer1_segment_constant_speed(void);
void led_test(void);
float read_temperature(void);
void read_and_parse_serial(void);

void go(void);
void halt(void);

void cambia_velocidad(void);
void cambia_direccion(void);
void cambia_pasos(void);
void cambia_setpoint(void);
void cambia_frecuencia(void);
void cambia_amplitud(void);

void ejecuta_programa(void);

void print_idn(void);
void print_vel(void);
void print_vals(void);
void print_pid(void);
void print_frec(void);

//void print_amp(void);




void (*jumpTable[])() = {dummy, timer1_constant_speed,  timer1_segment_constant_speed, timer2_wave};
