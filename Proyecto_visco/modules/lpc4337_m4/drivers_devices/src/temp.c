/* Control de Temperatura */


/*==================[inclusions]=============================================*/

//#include <FastPID.h>

#include "gpio.h"
#include "control_temp.h" //ojo no se si imprime el guion bajo

/*==================[macros and definitions]=================================*/

#define SERIESRESISTOR 4700
#define THERMISTORNOMINAL 100000
#define BCOEFFICIENT 3950
#define TEMPERATURENOMINAL 25
#define RATE 10
#define INV_RATE 0.1


/*==================[internal data definition]===============================*/

int16_t temp_array[32];
int16_t acum_temp;

float Kp=35, Ki=0, Kd=0;
int output_bits = 8;
bool output_signed = false;
byte power;

int16_t control_window = 128;

int16_t setpoint;


boolean newData = false;
const byte numChars = 16;
char receivedChars[numChars];
char tempChars[numChars];        // temporary array for use when parsing
char commandFromPC[numChars] = {0};
char parameterFromPC[numChars] = {0};


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/


/*==================[end of file]============================================*/




// FastPID myPID(Kp, Ki, Kd, RATE, output_bits, output_signed);




void recvWithStartEndMarkers() {
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '<';
    char endMarker = '>';
    char rc;

    while (Serial.available() > 0 && newData == false) {
        rc = Serial.read();

        if (recvInProgress == true) {
            if (rc != endMarker) {
                receivedChars[ndx] = rc;
                ndx++;
                if (ndx >= numChars) {
                    ndx = numChars - 1;
                }
            }
            else {
                receivedChars[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                ndx = 0;
                newData = true;
            }
        }

        else if (rc == startMarker) {
            recvInProgress = true;
        }
    }
}


void parseData() {      // split the data into its parts

    char * strtokIndx; // this is used by strtok() as an index

    strtokIndx = strtok(tempChars," ");      // get the first part - the comando
    strcpy(commandFromPC, strtokIndx); // copy it to commandFromPC
 
    strtokIndx = strtok(NULL, " "); // this continues where the previous call left off
    strcpy(parameterFromPC,strtokIndx);     // copy it to parameterFromPC
}


void print_vals(){ //en consola de arduino al preguntar <?> imprime valores de tiempo, temp y potencia
  Serial.print("<");
  Serial.print(millis());
  Serial.print(" ");
  Serial.print(acum_temp);
  Serial.print(" ");
  Serial.print(power);
  Serial.println(">");
  Serial.flush();
}

void print_pid(){ //en consola de arduino al preguntar <PID?> imprime las constantes kp, ki y kd
  Serial.print(Kp);
  Serial.print(" ");
  Serial.print(Ki);
  Serial.print(" ");
  Serial.println(Kd);
  Serial.flush();
}

void cambia_proporcional(){
  Kp = atof(parameterFromPC);
  myPID.setCoefficients(Kp,Ki,Kd,RATE);
}

void cambia_integral(){
  Ki = atof(parameterFromPC);
  myPID.setCoefficients(Kp,Ki,Kd,RATE);

}

void cambia_deriv(){
  Kd = atof(parameterFromPC);
  myPID.setCoefficients(Kp,Ki,Kd,RATE);
}

void cambia_setpoint(){
  setpoint =    atoi(parameterFromPC);
}

void read_and_parse_serial(){

recvWithStartEndMarkers();
if (newData == true) {
        strcpy(tempChars, receivedChars);
            // this temporary copy is necessary to protect the original data
            //   because strtok() used in parseData() replaces the commas with \0
        parseData();
        
  if (strcmp(commandFromPC,"?")==0)  print_vals();
  else if (strcmp(commandFromPC,"STP")==0)cambia_setpoint();
  else if (strcmp(commandFromPC,"KP")==0) cambia_proporcional();
  else if (strcmp(commandFromPC,"KI")==0) cambia_integral();
  else if (strcmp(commandFromPC,"KD")==0) cambia_deriv();
    
  else if (strcmp(commandFromPC,"PID?")==0)print_pid();
  else if (strcmp(commandFromPC,"STP?")==0)Serial.println(setpoint);
  
  else Serial.println("ERROR");
  newData = false;
  Serial.flush();
}
}


void setup() {
  Serial.begin(500000);
 for(int i=0;i < 32;i++){
  temp_array[i] = analogRead(THERMISTOR_PIN);
  acum_temp += temp_array[i];
} 

}

void loop() {

static unsigned long time_last_actualizacion;
static unsigned long time_actual;

static byte temp_counter;
 
time_actual = millis();

temp_counter++;
temp_counter &= 0b11111;
acum_temp -= temp_array[temp_counter];
temp_array[temp_counter] = analogRead(THERMISTOR_PIN);
acum_temp += temp_array[temp_counter];


if( (time_actual-time_last_actualizacion) >= INV_RATE){
  time_last_actualizacion = time_actual;
  
  if((setpoint-acum_temp) > control_window){
      power = 0;
    }
    else if((acum_temp-setpoint) > control_window){
      power =255;
  }
    else{
  // Como el termistor es NTC y el valor medido por el ADC es proporcional  la Resistencia del Termistor, entonces tengo que pasarle los valores multiplicados por -1
    power = myPID.step(-setpoint, -acum_temp);
    }
    analogWrite(HEATER_PIN, power );
}

if(Serial.available()>0) {
  read_and_parse_serial();
 }
}
