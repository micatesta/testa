#include "viscoelastometro.h"

uint8_t pasos[1024];
volatile uint8_t direcciones;

volatile long int globalpos=0;
volatile int8_t direccion;
volatile uint8_t isrSelect = 0;
float frecuencia;
int amplitud;
float velocidad; // variable que guarda la velocidad en pasos por segundo de los programas P0 y P1
unsigned int num_pasos = 0; //variable donde se guardan los pasos que aun quedan por recorrer en el segmento actual del programa P1

const int control_window =4;

float Kp=60, Ki=0, Kd=0;
int output_bits = 8;
int16_t setpoint = 950;
int controlando =1 ;
bool output_signed = false;

const byte numChars = 16;
char receivedChars[numChars];
char tempChars[numChars];        // temporary array for use when parsing

char commandFromPC[numChars] = {0};
char parameterFromPC[numChars] = {0};

boolean newData = false;


volatile unsigned int t_counter;
volatile unsigned int a_counter;
volatile byte bite,byteval;
volatile byte *tmppasos;


FastPID myPID(Kp, Ki, Kd, RATE, output_bits, output_signed);


long adc_presion;
byte gain_bits=1;

int16_t adc_temp;
int16_t acum_temp=0;
int16_t temp_array[32];
byte temp_counter=0;

void setup() {
  // put your setup code here, to run once:
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1B |= (1 << WGM12); // CTC mode 4
    TIMSK1 |= (1 << OCIE1A);

unsigned long t1,t2;

Serial.begin(500000);
amplitud = 5;
fill_pasos_seno(amplitud);
frecuencia = 0.1;
set_timer1_wave(frecuencia);
velocidad = F_CPU/counts_from_timer1(); //pasos por segundo)
num_pasos = 100;
pinMode(DIR_PIN, OUTPUT);
pinMode(STEP_PIN, OUTPUT);
pinMode(MS0_PIN, OUTPUT);
pinMode(MS1_PIN, OUTPUT);
pinMode(MS2_PIN, OUTPUT);
set_mustep(4);
pinMode(HX711_CLK, OUTPUT);
pinMode(HX711_DAT, INPUT_PULLUP);

for(int i=0;i < 32;i++){
  temp_array[i] = analogRead(THERMISTOR_PIN);
  acum_temp += temp_array[i];
}

}


void loop() {
static unsigned long time_last_actualizacion;
static unsigned long time_actual;
static byte power;
 

 

time_actual= millis();
//adc_presion=0;
adc_presion= ReadCount();
if(adc_presion == 0x7FFFFF || adc_presion == 0xFF800000 && gain_bits ==1) gain_bits = 3;
if(adc_presion <= 0x3FFFF0 || adc_presion >= 0xFFC00008 && gain_bits ==3) gain_bits = 1;

/* 
 *  Inicio de rutina para tener una variable con la sumade las últimas ocho mediciones del sensor de presion
static long p[8];
static byte cnt=0;
cnt = (cnt+1) & 0b11;
acum -= *(p+cnt);
*(p+cnt) = adc_presion;
acum += *(p+cnt);

*/

 if( (time_actual-time_last_actualizacion) >= INV_RATE){
  
  time_last_actualizacion = time_actual;
  
  temp_counter++;
  temp_counter &= 0b11111;
  acum_temp -= temp_array[temp_counter];
  temp_array[temp_counter] = analogRead(THERMISTOR_PIN);
  acum_temp += temp_array[temp_counter];
  adc_temp = acum_temp >> 5;
//  Serial.print(adc_temp);Serial.print(" "); Serial.println(temp_array[temp_counter]);
  if(controlando){
    if((setpoint-adc_temp) > control_window){
      power = 0;
    }
    else if((adc_temp-setpoint) > control_window){
      power =255;
    }
    else{
  // Como el termistor es NTC y el valor medido por el ADC es proporcional  la Resistencia del Termistor, entonces tengo que pasarle los valores multiplicados por -1
    power = myPID.step(-setpoint, -adc_temp);
    }
    analogWrite(HEATER_PIN, power );
   
  }
//   print_vals();
 // Serial.print(globalpos); Serial.print(" ");Serial.println(adc_presion);
 }
 
 if(Serial.available()>0) read_and_parse_serial();
}
