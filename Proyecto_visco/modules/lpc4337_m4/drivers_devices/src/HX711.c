/*
 * Maestría en Ingeniería Biomédica
 * Arquitectura y Programacion de Sistemas Embebidos
 * FIUNER - 2021
 * Autora: Micaela Testa
 * mica.testa@gmail.com
 *
 * Revisión:
 * -09-2021: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[objetivos]=============================================*/

/*

Driver para leer los datos de un sensor de presión diferencial, modelo PX26 Omega, dentro de un circuito de microfluídica. 

 */

/*==================[inclusions]=============================================*/

#include "HX711.h"       /* <= own header */
#include "systemclock.h"
#include <stdio.h>
#include <stdint.h>
#include "gpio.h"


/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/


void HX711_init(){

	GPIOInit(HX711_DAT, GPIO_INPUT);//Data
	GPIOInit(HX711_CLK, GPIO_OUTPUT);//clk
	
}


int32_t HX711_read(void)
{
	// wait for the chip to become ready
	//while (!HX711_is_ready());

    signed long count; 
    unsigned char i;
 
    GPIOOn(HX711_CLK);
    _delay_us(1);
    GPIOOff(HX711_CLK);
    _delay_us(1);
 
    count=0; 

    while(HX711_DAT); 
    for(i=0;i<24;i++)
    { 
        GPIOOn(HX711_CLK);
        _delay_us(1);
        count=count<<1; 
        GPIOOff(HX711_CLK);
        _delay_us(1);
        if(HX711_DAT)
            count++; 
    } 

    // Hacemos dos pulsos más para estar en el canal gain 32

    for (int j=0; j<3; j++){
    GPIOOn(HX711_CLK);
    _delay_us(1);
    GPIOOff(HX711_CLK);
      _delay_us(1);
    }
  
    count = count << 8;
    count = (int) count >> 8;

    return(count);
}

