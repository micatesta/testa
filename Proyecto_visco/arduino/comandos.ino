

void print_idn(void){
Serial.print(MODEL); Serial.print(" ");Serial.print(VERSION);Serial.print(" ");Serial.println(SERIE);
}


void cambia_velocidad(){
    volatile float v;
    v = atof(parameterFromPC);
    if (v == 0) 
      set_timer1(ULONG_MAX);
    else
      set_timer1((unsigned long)(1000000./abs(v))); // *******************
    direccion=1;
    if (v <0) direccion =-1;    
    set_direccion();   
  
}

void cambia_direccion(){
    volatile int d;
    d = atoi(parameterFromPC);
    if (d <= 0)
      direccion = -1; 
    else
      direccion=1;
    set_direccion();   
}

void cambia_pasos(){
  num_pasos = atoi(parameterFromPC);   
}

void cambia_setpoint(){
  setpoint = atoi(parameterFromPC);   
}

void cambia_frecuencia(){
    frecuencia = atof(parameterFromPC);
    set_timer1_wave(frecuencia);   
}

void cambia_amplitud(){
    amplitud = atoi(parameterFromPC);
    fill_pasos_seno(amplitud);
}

void cambia_proporcional(){
  Kp = atof(parameterFromPC);
  myPID.setCoefficients(Kp,Ki,Kd,RATE);
}

void cambia_integral(){
  Ki = atof(parameterFromPC);
  myPID.setCoefficients(Kp,Ki,Kd,RATE);
}

void cambia_deriv(){
  Kd = atof(parameterFromPC);
  myPID.setCoefficients(Kp,Ki,Kd,RATE);
}

void cambia_control(){
  controlando = atoi(parameterFromPC);
  if(controlando ==0) analogWrite(HEATER_PIN, 0 );
}

void ejecuta_programa(){
    volatile int d;
    d = atoi(parameterFromPC);

    if (d <=4 )
      isrSelect =d; 
    else 
      isrSelect = 0;
      
    go();   
}


void print_vel(){
  Serial.println(F_CPU/counts_from_timer1());
}

void print_frec(){
  Serial.println(F_CPU/counts_from_timer1()/8192.);
  }



void print_pid(){
  Serial.print(Kp);
  Serial.print(" ");
  Serial.print(Ki);
  Serial.print(" ");
  Serial.println(Kd);
}

void print_vals(){
  Serial.print("<");
  Serial.print(millis());
  Serial.print(" ");
  Serial.print(globalpos);
  Serial.print(" ");
  Serial.print(adc_presion);
  Serial.print(" ");
  Serial.print(adc_temp);
//  Serial.print(" ");
//  Serial.print(direccion);
  Serial.println(">");
  Serial.flush();
}


void read_and_parse_serial(){

recvWithStartEndMarkers();
if (newData == true) {
        strcpy(tempChars, receivedChars);
            // this temporary copy is necessary to protect the original data
            //   because strtok() used in parseData() replaces the commas with \0
        parseData();

  if (strcmp(commandFromPC,"?")==0)  print_vals();
  else if (strcmp(commandFromPC,"X?")==0)Serial.println(globalpos);
  else if (strcmp(commandFromPC,"STOP")==0)     halt();
  else if (strcmp(commandFromPC,"PAUSE")==0)    pause();
  
  else if (strcmp(commandFromPC,"RUN")==0) ejecuta_programa();
  else if (strcmp(commandFromPC,"N")==0)   cambia_pasos();
  else if (strcmp(commandFromPC,"DIR")==0) cambia_direccion();
  else if (strcmp(commandFromPC,"STP")==0)cambia_setpoint();
  else if (strcmp(commandFromPC,"FRE")==0)cambia_frecuencia();
  else if (strcmp(commandFromPC,"AMP")==0) cambia_amplitud();
  else if (strcmp(commandFromPC,"VEL")==0) cambia_velocidad();
  else if (strcmp(commandFromPC,"KP")==0) cambia_proporcional();
  else if (strcmp(commandFromPC,"KI")==0) cambia_integral();
  else if (strcmp(commandFromPC,"KD")==0) cambia_deriv();
  else if (strcmp(commandFromPC,"CNT")==0) cambia_control();
 
    
  else if (strcmp(commandFromPC,"VEL?")==0)print_vel();
  else if (strcmp(commandFromPC,"N?")==0)Serial.println(num_pasos);
  else if (strcmp(commandFromPC,"RUN?")==0)Serial.println(isrSelect);
  else if (strcmp(commandFromPC,"DIR?")==0)Serial.println(direccion);
  else if (strcmp(commandFromPC,"FRE?")==0)print_frec();
  else if (strcmp(commandFromPC,"AMP?")==0)Serial.println(amplitud);
  else if (strcmp(commandFromPC,"PID?")==0)print_pid();
  else if (strcmp(commandFromPC,"STP?")==0)Serial.println(setpoint);
  else if (strcmp(commandFromPC,"CNT?")==0)Serial.println(controlando);
  
  else Serial.println("ERROR");
//  commandFromPC[0] = 0;
//  parameterFromPC[0] =0;
  newData = false;
  Serial.flush();
}  
  }
